#!/bin/bash

sleep 1s

echo "Some long output which should be hidden in the report for a successful test, but visible if the test fails to facilitate debugging"

# fail the test
exit 1
