# Demonstration of a failing test


Describe 'app2'
  It 'runs'
    When run ./app2.sh
    The status should be success

    # NOTE: I do not want to test output here, but even with a flag to suppress
    # failure on warnings I still get the warnings in the report!
  End
End
